import setuptools
with open("README.md", "r") as fh:
   long_description = fh.read()
setuptools.setup(
   name="fbx-SolleXelloS",
   version="0.0.1",
   author="Evan McCall",
   author_email="evan@digital-giant.com",
   description="FBX Wrapper Package for Python 2.7",
   long_description=long_description,
   long_description_content_type="text/markdown",
   url="https://bitbucket.org/SolleXelloS/fbxsdkpythonbinding27-package-dist/src/master/", 
   packages=setuptools.find_packages(),
   classifiers=[
       "Programming Language :: Python :: 2", # Replace with Python Interpretter
       "License :: OSI Approved :: MIT License",
       "Operating System :: OS Independent",
   ],
   python_requires='>=2.7', # Replace with Python Interpretter 
)
